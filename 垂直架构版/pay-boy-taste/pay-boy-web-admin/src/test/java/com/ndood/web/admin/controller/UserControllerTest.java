package com.ndood.web.admin.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.ndood.admin.pojo.system.dto.UserDto;
import com.ndood.admin.pojo.system.query.UserQuery;
import com.ndood.admin.repository.system.manager.UserRepositoryManager;

/**
 * 新建测试类
 * @author ndood
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {
	
	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Autowired
	private UserRepositoryManager userRepositoryManager;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/**
	 * test初始化
	 */
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	/**
	 * 测试查询请求测试用例
	 * @throws Exception 
	 */
	@Test
	public void whenQuerySuccess() throws Exception{
		String result = mockMvc.perform(MockMvcRequestBuilders.get("/user")
				.param("username", "jojo")
				.param("age", "18")
				.param("ageTo", "60")
				.param("xxx", "yyy")
				/*.param("size", "15")
				.param("page", "3")
				.param("sort", "age,desc")*/
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码 OK 200
				.andExpect(status().isOk())
				// 返回的list长度是3 https://github.com/json-path/JsonPath
				.andExpect(jsonPath("$.length()").value(3))
				// 打印返回结果
				.andReturn().getResponse().getContentAsString();
		System.out.println(result);
	}
	
	/**
	 *  测试获取单个用户信息
	 * @throws Exception 
	 */
	@Test
	public void whenGetInfoSuccess() throws Exception{
		String result = mockMvc.perform(get("/user/1")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username").value("tom"))
				// 打印返回结果
				.andReturn().getResponse().getContentAsString();
		System.out.println(result);
	}
	
	/**
	 * 测试获取用户信息失败
	 * @throws Exception 
	 */
	@Test
	public void whenGetInfoFail() throws Exception{
		mockMvc.perform(get("/user/a")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().is4xxClientError());
	}
	
	/**
	 * 测试创建用户
	 * @throws Exception 
	 */
	@Test
	public void whenCreateSuccess() throws Exception{
		Date date = new Date();
		System.out.println(date.getTime());
		String content = "{\"username\":\"tom\",\"password\":null,\"birthday\":\""+date.getTime()+"\"}";
		String result = mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value("1"))
				.andReturn().getResponse().getContentAsString();
		System.out.println(result);
	}
	
	/**
	 * 测试修改用户
	 * @throws Exception
	 */
	@Test
	public void whenUpdateSuccess() throws Exception{
		// jdk8 日期新特性
		Date date = new Date(LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		System.out.println(date.getTime());
		String content = "{\"id\":\"1\",\"username\":\"tom\",\"password\":null,\"birthday\":\""+date.getTime()+"\"}";
		String result = mockMvc.perform(put("/user/1").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value("1"))
				.andReturn().getResponse().getContentAsString();
		System.out.println(result);
	}
	
	/**
	 * 测试删除用户
	 * @throws Exception
	 */
	@Test
	public void whenDeleteSuccess() throws Exception{
		mockMvc.perform(delete("/user/1")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
	}
	
	/**
	 * 测试文件上传
	 * @throws Exception 
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void whenUploadSuccess() throws Exception{
		String result = mockMvc.perform(fileUpload("/file")
				.file(new MockMultipartFile("file", "test.txt", "multipart/form-data", "hello upload".getBytes("UTF-8"))))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		System.out.println(result);	
	}
	
	@Test
	@Transactional
	public void testDao() throws Exception{
		UserQuery query = new UserQuery();
		
		query.setKeywords("aa");
		query.setLimit(15);
		query.setOffset(0);
		int pageNo = query.getPageNo();
		System.out.println(pageNo+"+++");
		
		Page<UserDto> page = userRepositoryManager.pageUserList(query);
		
		System.out.println(page.getTotalElements());
		System.out.println(page.getContent().get(0).getProvinceId());
	}
	
	@Test
	public void testPasswordEncoder() {
		/*String encode = passwordEncoder.encode("123456");
		System.out.println(encode);
		encode = passwordEncoder.encode("123456");
		System.out.println(encode);
		*/
		System.out.println(passwordEncoder.matches("$2a$10$gjix4nzgYcKizFCOn5ZdVu9BAi3I.KT7jqjej/0dHuxET6p9KkMwq","123456"));
	}
}
