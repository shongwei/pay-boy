package com.ndood.admin.pojo.system.dto;

import java.util.Date;

import com.ndood.admin.pojo.system.LogPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 日志DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class LogDto extends LogPo {
	private static final long serialVersionUID = 2000231857406108177L;

	public LogDto() {
		super();
	}
	
	public LogDto(Integer id, String ip, String ipInfo, String requestType, String requestUrl, String requestParam,
			String username, Date createTime) {
		super();
		this.id = id;
		this.ip = ip;
		this.ipInfo = ipInfo;
		this.requestType = requestType;
		this.requestUrl = requestUrl;
		this.requestParam = requestParam;
		this.username = username;
		this.createTime = createTime;
	}
	
	/**
	 * id
	 */
	private Integer id;
	/**
	 * ip
	 */
	private String ip;
	/**
	 * ip信息
	 */
	private String ipInfo;
	/**
	 * 请求类型
	 */
	private String requestType;
	/**
	 * 请求地址
	 */
	private String requestUrl;
	/**
	 * 请求参数
	 */
	private String requestParam;
	/**
	 * 账户名
	 */
	private String username;
	/**
	 * 创建时间
	 */
	private Date createTime;
	
}
