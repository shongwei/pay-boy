package com.ndood.admin.pojo.comm.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AdminErrorVo implements Serializable {
	private static final long serialVersionUID = 4948544702730668719L;

	private String code;
	private String msg;

	public AdminErrorVo() {
		super();
	}

	public AdminErrorVo(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

}
