package com.ndood.admin.repository.system;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.PermissionPo;

/**
 * https://blog.jooq.org/tag/sqlresultsetmapping/
 */
public interface PermissionRepository extends JpaRepository<PermissionPo, Integer> {

	List<PermissionPo> findByOrderBySort();
	
	List<PermissionPo> findByParent(PermissionPo po);
}
