package com.ndood.admin.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.dto.ConfigDto;
import com.ndood.admin.service.system.impl.SystemConfigServiceImpl;

@Controller
public class SystemConfigController {
	@Autowired
	private SystemConfigServiceImpl systemConfigService;

	@GetMapping("/system/config")
	public String toAccountInfo(Model model) throws Exception{
		return "system/config/config_page";
	}
	
	/**
	 * 修改用户
	 */
	@PostMapping("/system/config/update")
	@ResponseBody
	public AdminResultVo updateUser(@RequestBody ConfigDto config) throws Exception{
		systemConfigService.updateConfig(config);
		return AdminResultVo.ok().setMsg("修改系统配置成功！");
	}

}
