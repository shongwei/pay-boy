package com.ndood.admin.controller.merchant;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 商户入驻
 */
@Controller
public class MerchantEnterController {
	
	/**
	 * 跳转到商户入驻页
	 */
	@GetMapping("/merchant/enter")
	public String toMechantEnterPage() {
		return "merchant/enter/enter_page";
	}
	
}
