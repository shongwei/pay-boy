/*package com.ndood.admin.core.web.aspect;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

*//**
 * 创建自定义切片类
 * 过滤器，拦截器，切片的区别：
 * 过滤器可以拿到请求体
 * 拦截器可以拿到响应的值，请求方法
 * 切片可以拿到请求参数
 * 
 * DispacherServlet 962行
 * 方法参数的拼装是在ha.hanle完成的，所以在拦截器的preHanle不知道参数是什么
 * @author ndood
 *//*
@Aspect
@Component
public class TimeAspect {
	
	*//**
	 * 声明一个切入点
	 * https://docs.spring.io/spring/docs/4.3.12.RELEASE/spring-framework-reference/htmlsingle/
	 * @throws Throwable 
	 *//*
	@Around("execution(* com.ndood.admin.controller.*.UserAuthController.*(..))")
	// 方法也就是“增强”
	public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable{
		//System.out.println("time aspect start");
		Object[] args = pjp.getArgs();
		for (Object arg : args) {
			//System.out.println("arg is " + arg);
		}
		long start = new Date().getTime();
		Object object = pjp.proceed();
		//System.out.println("time aspect 耗时:"+(new Date().getTime()-start));
		//System.out.println("time aspect end");
		return object;
	}
	
}
*/