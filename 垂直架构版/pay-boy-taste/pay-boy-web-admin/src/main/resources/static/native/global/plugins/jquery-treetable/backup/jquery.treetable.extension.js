// 官网：http://ludo.cubicphuse.nl/jquery-treetable/
(function($) {
	"use strict";
	// 自制的支持treetable的组件，像bootstrap table那样，传入url，data之类的数据自动生成treetable.并影响应对应的事件。可以根据formmater格式化
	$.fn.ndoodTreeTable = function(options, param, param2) {

		// 如果options为方法，则调用字符串对应的方法
		if (typeof options == 'string') {
			return $.fn.ndoodTreeTable.methods[options](this, param, param2);
		}

		// 如果options为json，则根据json进行初始化
		options = $.extend({}, $.fn.ndoodTreeTable.defaults, options || {});
		var target = $(this);
		target.prop('temp_options',options);
		
		// 显示加载中效果
		target.addClass("treetable");
		var loading_thead = makeTHeadView(options);
		var loading_tbody = $("<tbody><tr style='text-align:center;background-color: #fbfcfd;'><td class='result_none' colspan='"+options.columns.length+"'>数据加载中，请耐心等待...</td></tr></tbody>")
		target.append(loading_thead);
		target.append(loading_tbody);

		// 延时加载，提高流畅度
		// ajax加载数据并生成树结构
		$.ajax({
			type : options.type,
			url : options.url,
			data : options.data,
			dataType : "JSON",
			async: true,
			success : function(data, textStatus, jqXHR) {
				target.html('');
				var thead = makeTHeadView(options);
				target.append(thead);
				
				// 没找到匹配的记录
				var list = data;
				if(list==null||list==undefined||list.length==0){
					var tbody = $("<tbody><tr style='text-align:center;background-color: #fbfcfd;'><td class='result_loading' colspan='"+options.columns.length+"'>没有找到匹配的记录</td></tr></tbody>");
					target.append(tbody);
					return;
				}
				
				// 获取id列表，用来后面判断parentNode是否存在（搜索的时候可能出现不存在的情况）
				var ids = Array();
				for(var i=0;i<list.length;i++){
					ids.push(list[i][options.id]);
				}
				ids = ','+ids.join(",")+",";
				
				// 生成tbody
				var tbody = makeTBodyView(options, list, ids);
				target.append(tbody);

				target.treetable({
					nodeIdAttr : options.nodeIdAttr,
					parentIdAttr : options.parentIdAttr,
					column : options.expandIndex,
					stringCollapse : options.stringCollapse,
					stringExpand : options.stringExpand,
					expandable : options.expandable,
					// 展开的时候根据排序字段进行排序
	                onNodeExpand: function(){  
	                	var node = this;
	                    var id = node.id;
	                    // 这里不能用node.hasBranch方法判断是否是分支，因为有可能是异步加载子节点
	                    var isBranch = target.find("tr[data-node-id='"+id+"']").attr('data-tt-branch') != undefined;
	                    //是否已经加载  
	                    if(isBranch && node.children && !node.children.length){  
	                    	// 加载该节点下的所有子节点
	                    	var subNodesHtml = loadSubNodesView(options, node.id);
	                        target.treetable("loadBranch",node, subNodesHtml);
	                        if(options.sortIndex!=null&&options.sortable){
		        				target.treetable("sortBranch", node, options.sortIndex);
		        			}
	                    }
	                }
				});
				return false;
			}
		});
		return target;
	};

	// 默认配置
	$.fn.ndoodTreeTable.defaults = {
		sortIndex : 1, // 根据第2列排序
		expandIndex : 1, // 第2列显示折叠标记
		id: "id", // 数据ID
		parentId : "parentId", // 数据parentId
		sortable : false, // 是否前端排序
		isLeaf : 'isLeaf', // 0 不是叶子节点， 1 是叶子节点
		type : "GET",
		url : null,
		data : {},
		columns : [],

		stringCollapse : "收起",
		stringExpand : "展开",
		nodeIdAttr : "nodeId",
		parentIdAttr : "parentId",
		expandable : true
	};
	
	// 画出thead
	function makeTHeadView(options) {
		var thead = $('<thead></thead>');
		var thr = $('<tr></tr>');
		
		$.each(options.columns, function(i, column) {
			var th = $('<th></th>');
			if (column.width != undefined) {
				th.css("width", column.width);
			}
			if (column.align != undefined) {
				th.css("text-align", column.align);
			}
			if (column.visible != undefined && column.visible == false) {
				th.css("display", "none");
			}
			th.text(column.title);
			thr.append(th);
		});
		thead.append(thr);
		
		return thead;
	}

	// 画出tbody
	function makeTBodyView(options, data, ids) {
		var tbody = $('<tbody></tbody>');
		
		$.each(data, function(i, row) {
			// 如果parentId不存在，或者parentId存在但parentId对应的row不存在（搜索的时候可能出现）
			var parentId = row[options.parentId]; 
			if (parentId ==undefined | ids.indexOf(","+parentId+",")==-1) {
				// 渲染单个节点视图
				var tr = makeThisView(options, row);
				tbody.append(tr);
				
				// 递归生成所有子节点对应的view（搜索的时候可能出现）
				makeNodesView(options, data, row, tbody);
			}
		});
		
		return tbody;
	}
	
	// 渲染单个节点
	function makeThisView(options, row) {
		var tr = $('<tr></tr>');
		
		// 添加 data-node-id属性
		tr.attr('data-node-id', row[options.id]);
		
		// 添加data-parent-id属性
		var parentId = row[options.parentId];
		if (parentId != undefined) {
			tr.attr('data-parent-id', parentId);
		}
		
		// isLeaf 1: 如果isLeaf有定义，则根据isLeaf来判断是否是叶子节点，添加data-tt-branch属性，拥有该属性的节点会显示折叠按钮
		var isLeaf= row[options.isLeaf];
		if(isLeaf != undefined && isLeaf==0 ){
			tr.attr("data-tt-branch", true);
		}
		
		// 生成节点所有的列，支持formatter格式化
		$.each(options.columns, function(i, column) {
			var td = $('<td></td>');
			if (column.width != undefined) {
				td.css("width", column.width);
			}
			if (column.align != undefined) {
				td.css("text-align", column.align);
			}
			if (column.visible != undefined && column.visible == false) {
				td.css("display", "none");
			}
			if (column.formatter != undefined) {
				var div = $('<font class="td_font"></font>');
				var formatted_value = column.formatter(row[column.field], row);
				div.append(formatted_value);
				td.append(div);
				tr.append(td);
			} else {
				var div = $('<font class="td_font"></font>');
				div.text(row[column.field]);
				td.append(div);
				tr.append(td);
			}
		});
		return tr;
	}
	
	// 递归遍历，然后生成节点
	function makeNodesView(options, data, parentNode, tbody) {
		// 获取parentId，用来加载子节点
		var parentId = parentNode[options.id]==null? null:parentNode[options.id];
		
		// 用来判断该节点是否是叶子节点
		var count = 0;
		$.each(data, function(i, row) {
			// 不是parentNode的子节点，不添加在该节点下面
			if (row[options.parentId] != parentId) {
				return;
			}
			var tr = makeThisView(options, row);
			tbody.append(tr);
			count++;
			
			// 递归遍历子节点（搜索的时候可能出现）
			makeNodesView(options, data, row, tbody);
		});
		
		// isLeaf 2: 如果isLeaf未定义，则根据父节点下是否有子节点判断父节点是否是分支
		var isLeaf = parentNode[options.isLeaf];
		if (isLeaf == undefined && count>0) {
			tbody.find("tr[data-node-id='" + parentId + "']").attr("data-tt-branch", true);
		}
	};
	
	// 添加子节点的view
	function loadSubNodesView(options, parentId){
		var html = $('<div></div>');
		var data = {} || options.data;
		data[options.parentId] = parentId;
		$.ajax({
			type : options.type,
			url : options.url,
			data : data,
			dataType : "JSON",
			async: false,
			success : function(data, textStatus, jqXHR) {
				$.each(data,function(i, row){
					var tr = makeThisView(options, row);
					html.append(tr);
	            });
				return false;
			}
		});
		return html.html();
	}
	
	// 自定义方法列表
	$.fn.ndoodTreeTable.methods = {
		// 重新加载内容
		reload : function(target, data){
			// 重新加载数据
			var temp_options = target.prop('temp_options');
			var options = temp_options;
			options.data = data; 
			target.treetable('destroy');
			target.html('').addClass("treetable");
			
			// 显示加载中效果
			var loading_thead = makeTHeadView(options);
			var loading_tbody = $("<tbody><tr style='text-align:center;background-color: #fbfcfd;'><td class='result_none' colspan='"+options.columns.length+"'>数据加载中，请耐心等待...</td></tr></tbody>")
			target.append(loading_thead);
			target.append(loading_tbody);

			// ajax加载数据并生成树结构
			$.ajax({
				type : options.type,
				url : options.url,
				data : options.data,
				dataType : "JSON",
				async: false,
				success : function(data, textStatus, jqXHR) {
					target.html('');
					var thead = makeTHeadView(options);
					target.append(thead);
					
					// 没找到匹配的记录
					var list = data;
					if(list==null||list==undefined||list.length==0){
						var tbody = $("<tbody><tr style='text-align:center;background-color: #fbfcfd;'><td class='result_loading' colspan='"+options.columns.length+"'>没有找到匹配的记录</td></tr></tbody>");
						target.append(tbody);
						return;
					}
					
					// 获取id列表，用来后面判断parentNode是否存在（搜索的时候可能出现不存在的情况）
					var ids = Array();
					for(var i=0;i<list.length;i++){
						ids.push(list[i][options.id]);
					}
					ids = ','+ids.join(",")+",";
					
					// 生成tbody
					var tbody = makeTBodyView(options, list, ids);
					target.append(tbody);

					target.treetable({
						nodeIdAttr : options.nodeIdAttr,
						parentIdAttr : options.parentIdAttr,
						column : options.expandIndex,
						stringCollapse : options.stringCollapse,
						stringExpand : options.stringExpand,
						expandable : options.expandable,
						// 展开的时候根据排序字段进行排序
		                onNodeExpand: function(){  
		                    var node = this;
		                    var id = node.id;
		                    // 这里不能用node.hasBranch方法判断是否是分支，因为有可能是异步加载子节点
		                    var isBranch = target.find("tr[data-node-id='"+id+"']").attr('data-tt-branch') != undefined;
		                    
		                    //是否已经加载  
		                    if(isBranch && node.children && !node.children.length){  
		                    	// 加载该节点下的所有子节点
		                    	var subNodesHtml = loadSubNodesView(options, node.id);
		                        target.treetable("loadBranch",node, subNodesHtml);
		                        if(options.sortIndex!=null && options.sortable){
			        				target.treetable("sortBranch", node, options.sortIndex);
			        			}
		                    }
		                }
					});
					return false;
				}
			});
			return target;
		},
		
		// 添加子节点 
		addBranch : function(target, row){
			var temp_options = target.prop('temp_options');
			var parentId = row[temp_options.parentId];
			var node = target.treetable("node", parentId);
			
			var html = makeThisView(temp_options, row);
			target.treetable("loadBranch", node, html);
			
			// 添加后进行排序
			if(temp_options.sortIndex!=null&&temp_options.sortable){
				target.treetable("sortBranch", node, temp_options.sortIndex);
			}
		},
		
		// 修改节点
		modifyNode : function(target, row) {
			var temp_options = target.prop('temp_options');
			var node = target.treetable("node", row[temp_options.id]);
			var pNode = target.treetable("node", row[temp_options.parentId]);
			$.each(temp_options.columns, function(i, column) {
				// 对于id和parentId的列不作处理
				if(column.field==temp_options.id||column.field==temp_options.parentId){
					return
				}
				var span0, span1;
				var td_font = node.row.find('td:eq('+i+')').find('.td_font');
				// 清空内容
				td_font.html('');
				// 对于formatter格式的，先formatter后再替换
				if (column.formatter != undefined) {
					var formatted_value = column.formatter(row[column.field], row);
					td_font.append(formatted_value);
				}else{
					td_font.text(row[column.field]);
				}
			});
			// 修改后进行排序
			if(temp_options.sortIndex!=null&&temp_options.sortable){
				target.treetable("sortBranch", pNode, temp_options.sortIndex);
			}
		},
		
		// 删除节点
		removeNode : function(target, id) {
			var parentId = $(target).find("tr[data-node-id='" + id + "']").attr('data-parent-id');
			var node = target.treetable("node", parentId);
			target.treetable("removeNode", id);
			// 如果是根节点则不继续处理
			if(parentId==undefined){
				return;
			}
			// 删除之后如果父节点没有子节点了，则将父节点的文件夹图标编程文件
			var count = $(target).find("tr[data-parent-id='" + parentId + "']").size();
			if(count==0){
				$(target).find("tr[data-node-id='" + parentId + "']").removeClass("branch").addClass("leaf");
				$(target).find("tr[data-node-id='" + parentId + "']").removeAttr("data-tt-branch");
				$(target).find("tr[data-node-id='" + parentId + "']").find(".indenter").html('');
			}
		},
		
		// 展开所有
		expandAll : function(target, id){
			target.treetable("expandAll");
		},
		
		// 收缩所有
		collapseAll : function(target){
			target.treetable("collapseAll");
		},
		
		// 展开子节点
		expandNode : function(target, id) {
			target.treetable("expandNode", id);
		},
		
		// 收缩子节点
		collapseNode : function(target, id) {
			target.treetable("collapseNode", id);
		},
		
		// 是否有子节点，这里不能用node.hasBranch方法判断，因为有可能是异步加载子节点
		hasSubNodes : function(target, id){
			// 如果不是叶子节点，则说明该节点下面还有子节点
			var isBranch = target.find("tr[data-node-id='"+id+"']").attr('data-tt-branch') != undefined;
			return isBranch;
		}
		
	};
	
})(jQuery);