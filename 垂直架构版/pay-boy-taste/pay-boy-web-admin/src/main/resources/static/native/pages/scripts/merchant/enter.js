define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-imguploader'
], function() {
	return {
		initPage: function(){
			
			// 经营许可证
			$("#merchant_enter_licence_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'licenceImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 开户许可证
			$("#merchant_enter_opening_permit_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'openingPermitImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 身份证正面
			$("#merchant_enter_id_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'idImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 身份证反面
			$("#merchant_enter_id_reverse_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'idReverseImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 税务登记证
			$("#merchant_enter_tax_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'taxImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 组织机构代码证
			$("#merchant_enter_org_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'orgImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 授权文件
			$("#merchant_enter_authorize_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'authorizeImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 商户logo
			$("#merchant_enter_logo_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'logoImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 其它资料1
			$("#merchant_enter_other1_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'other1ImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 其它资料2
			$("#merchant_enter_other2_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'other2ImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 其它资料3
			$("#merchant_enter_other3_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'other3ImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 其它资料4
			$("#merchant_enter_other4_img").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				width: '150px',
				height: '100px',
				imgName: 'other4ImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
		}
	}
});